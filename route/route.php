<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});

//Route::get('hello/:name', 'index/hello');
Route::get('', 'index/index/home');
Route::get('home', 'index/index/home');
Route::get('configmanage', 'index/index/configmanage');
Route::get('createconfig', 'index/index/createconfig');
Route::get('updateconfig', 'index/index/updateconfig');
Route::get('valuetest', 'index/index/valuetest');
Route::get('set', 'index/index/set');


Route::rule('getconfigvalue', 'index/api/getconfigvalue');

return [

];
