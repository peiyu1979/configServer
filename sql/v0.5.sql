create database configserver;
use configserver;


CREATE TABLE `config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `value` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `value_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `app` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `runmodel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `rem` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `createtime` datetime(0) NULL DEFAULT NULL,
  `updatetime` datetime(0) NULL DEFAULT NULL,
  `looktime` datetime(0) NULL DEFAULT NULL,
  `lookcount` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态：0正常，1停用',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip',
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '版本',

  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key`(`key`) USING BTREE
) ENGINE = InnoDB  CHARACTER SET = utf8 COLLATE = utf8_general_ci ;


delimiter ;;
CREATE TRIGGER `config_setcreatetime` BEFORE INSERT ON `config` FOR EACH ROW set new.createtime=NOW()
;;

CREATE TRIGGER `config_setupdatetime` BEFORE UPDATE ON `config` FOR EACH ROW if new.`value` != old.`value` then
set new.updatetime=NOW() ;
end if
;;
delimiter ;

-- ----------------------------
-- Records of config
-- ----------------------------


-- ----------------------------
-- Table structure for dictionary
-- ----------------------------

CREATE TABLE `dictionary`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `rem` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `createtime` datetime(0) NULL DEFAULT NULL,
  `updatetime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `key`(`key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ;

delimiter ;;
CREATE TRIGGER `dictionary_setcreatetime` BEFORE INSERT ON `dictionary` FOR EACH ROW set new.createtime=NOW()
;;

CREATE TRIGGER `dictionary_setupdatetime` BEFORE UPDATE ON `dictionary` FOR EACH ROW if new.`value` != old.`value` then
set new.updatetime=NOW() ;
end if
;;
delimiter ;

-- ----------------------------
-- Records of dictionary
-- ----------------------------
INSERT INTO `dictionary`( `key`, `value`, `rem`) VALUES ('value_type', '{\"string\":\"string\",\"int\":\"int\",\"float\":\"float\",\"array\":\"array\",\"json\":\"json\"}', '值类型');
INSERT INTO `dictionary`( `key`, `value`, `rem`) VALUES ('dimension_black', '[\"id\",\"key\",\"value\",\"value_type\",\"rem\",\"createtime\",\"updatetime\",\"looktime\",\"lookcount\",\"status\"]', '维度黑名单');

-- ----------------------------
-- Table structure for dimension
-- ----------------------------

CREATE TABLE `dimension`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sortby` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '优先级',
  `if_free` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否是自由模式：1 是 ，2 否',
  `values` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '如果不是自由模式，值的可选项，json数组',
  `createtime` datetime(0) NULL DEFAULT NULL,
  `updatetime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `key`(`key`) USING BTREE
) ENGINE = InnoDB  CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '维度' ;

delimiter ;;
CREATE TRIGGER `dimension_setcreatetime` BEFORE INSERT ON `dimension` FOR EACH ROW set new.createtime=NOW()
;;

CREATE TRIGGER `dimension_setupdatetime` BEFORE UPDATE ON `dimension` FOR EACH ROW set new.updatetime=NOW()
;;
delimiter ;
-- ----------------------------
-- Records of dimension
-- ----------------------------
INSERT INTO `dimension`(`key`, `title`, `sortby`, `if_free`, `values`) VALUES ('app', '应用', 0, 2, '[\"\",\"user\",\"order\"]');
INSERT INTO `dimension`(`key`, `title`, `sortby`, `if_free`, `values`) VALUES ('runmodel', '运行方式', 1, 2, '[\"\",\"debug\",\"release\"]');
INSERT INTO `dimension`(`key`, `title`, `sortby`, `if_free`, `values`) VALUES ('ip', 'ip', 2, 1, '');
INSERT INTO `dimension`(`key`, `title`, `sortby`, `if_free`, `values`) VALUES ('version', '版本', 3, 1, '');


-- ----------------------------
-- Table structure for keydepend
-- ----------------------------

CREATE TABLE `keydepend`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key_up` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上游键',
  `key_down` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '下游键',
  `createtime` datetime(0) NULL DEFAULT NULL,
  `updatetime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `key_up`(`key_up`, `key_down`) USING BTREE
) ENGINE = InnoDB  CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '健依赖关系' ;

delimiter ;;
CREATE TRIGGER `keydepend_setcreatetime` BEFORE INSERT ON `keydepend` FOR EACH ROW set new.createtime=NOW()
;;

CREATE TRIGGER `keydepend_setupdatetime` BEFORE UPDATE ON `keydepend` FOR EACH ROW set new.updatetime=NOW()
;;
delimiter ;
-- ----------------------------
-- Records of keydepend
-- ----------------------------

---------  0.5 ->0.6

UPDATE `dictionary` SET  `value` = '{\"string\":\"string\",\"int\":\"int\",\"float\":\"float\",\"array\":\"array\",\"json\":\"json\",\"code\":\"code\"}'  WHERE `key` = 'value_type' ;

-- 禁用 code
-- UPDATE `dictionary` SET  `value` = '{\"string\":\"string\",\"int\":\"int\",\"float\":\"float\",\"array\":\"array\",\"json\":\"json\"}'  WHERE `key` = 'value_type' ;
