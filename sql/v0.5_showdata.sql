
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('domain_name', 'test.com', 'string', '', '', '主域名', 0, '', '');
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('host_name', 'www.${domain_name}', 'string', '', '', '主域名', 0, '', '');
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('host_name', 'order.${domain_name}', 'string', 'order', '', '订单模块域名', 0, '', '');
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('host_name', 'order-test.${domain_name}', 'string', 'order', 'debug', '订单模块测试域名', 0, '', '');
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('order_price', '21.34', 'float', 'order', '', '', 0, '', '');
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('order_price', '26.34', 'float', 'order', 'debug', '', 1, '', '0.4');
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('order_status', '{\"wait_rec\":\"待揽收\",\"send\":\"配送中\",\"complete\":\"完成\"}', 'json', 'order', '', '', 0, '', '');
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('timeout', '3000', 'int', '', '', '', 0, '', '');
INSERT INTO `config`(`key`, `value`, `value_type`, `app`, `runmodel`, `rem`, `status`, `ip`, `version`) VALUES ('timeout', '10000', 'int', '', '', '', 0, '192.168.2.3', '');



INSERT INTO `keydepend`(key_up,key_down) VALUES ( 'domain_name', 'host_name');

