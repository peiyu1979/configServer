<?php
/**
 * Created by PhpStorm.
 * User: peiyu
 * Date: 2019/7/3
 * Time: 14:46
 */

namespace app\common\model;

use think\Model;
use Log;

class keydepend extends Model
{

    public static function addDepend($key_up,$key_down){


        $_k= self::where("key_up",$key_up)->where("key_down",$key_down)->find();
        if($_k)return ;
        self::create(["key_up"=>$key_up,"key_down"=>$key_down]);


    }

    public static function delDepend($key_up,$key_down){
        $_k= self::where("key_up",$key_up)->where("key_down",$key_down)->find()->toArray();
        if(!$_k)return ;
        Log::debug($_k);
        self::where("id",$_k["id"])->delete();
    }

    //获得我的上游依赖
    public static function getMyUpDepend($key_down){
        return  self::where("key_down",$key_down)->select()->toArray();

    }

    //获得我的下游依赖
    public static function getMyDownDepend($key_up){
        return  self::where("key_up",$key_up)->select()->toArray();

    }

}