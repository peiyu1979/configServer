<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

function array_get(array $array, $key, $default = null)
{
    if (is_null($key)) {
        return $array;
    }

    if (isset($array[$key])) {
        return $array[$key];
    }

    foreach (explode('.', $key) as $segment) {
        if (!is_array($array) || !array_key_exists($segment, $array)) {
            return ($default);
        }

        $array = $array[$segment];
    }
    return $array;
}

function array_only_key(array $array, array $keys)
{
    return array_filter($array, function ($v, $k) use ($keys) {
        return in_array($k, $keys);
    }, ARRAY_FILTER_USE_BOTH);
}

// 计算权重
function computeWeight(array $array, array $keys)
{

    $kvs = [];
    $c_keys = count($keys);
    foreach ($keys as &$k) {
        $_ks = array_unique(array_column($array, $k));
        sort($_ks);
        $kvs[$k] = $_ks;
    }
    Log::debug(json_encode($kvs, JSON_UNESCAPED_UNICODE));

    foreach ($array as &$item) {
        $_w = 0;
        foreach ($keys as $i => &$k) {
            if (!isset($item[$k])) continue;
            $_v = $item[$k];
            $vs = $kvs[$k];
            $_k = (int)array_search($_v, $vs);
            $_w_ = pow(100, ($c_keys - (int)$i)) * $_k;
            $_w = $_w + $_w_;
            $item["w_" . $k] = $_w_;
        }
        $item["weight"] = $_w;
    }
    Log::debug(json_encode($array, JSON_UNESCAPED_UNICODE));

    usort($array, function ($a, $b) {
        if ($a["weight"] == $b["weight"]) {
            return 0;
        }
        return ($a["weight"] < $b["weight"]) ? -1 : 1;
    });
    Log::debug(json_encode($array, JSON_UNESCAPED_UNICODE));
    return $array;
}

function contentFormat($content, array $data)
{

    if (!$data || !$content) return $content;
    foreach ($data as $key => $item) {

        $content = str_replace('${' . $key . '}', $item, $content);
    }

    Log::debug($content);
    return $content;
}

function getVarFromContent($content, $t = 'no$')
{

    if (!$content || !is_string($content)) return null;
    $preg = '/\$\{([^\}]+)\}/';
    $c = preg_match_all($preg, $content, $matches);
    Log::debug(json_encode([$content, $matches], JSON_UNESCAPED_UNICODE));
    if ($c) return $t == 'no$' ? $matches[1] : $matches;
    return null;
}

/**
 *      * 读取一个表的列信息
 *      * @author 吾爱 qq296624314
 *      * @param string $tableName 表名
 *      * @param array $option 需要获取的属性
 *      * @return array 返回的一个数组，若指定属性，则以 array("列名1"=>array("属性名1"=>"属性1值"……)……)的格式返回，否则以 array("列名1","列名2"……)的格式返回
 *      * @example 
 *      */
function getcolumns($tableName, $option = array())
{

    $columns = array();
    $m_re = Db::query("show columns from `{$tableName}`");
    if (!$m_re) {
        return array();
    }
    foreach ($m_re as $v) {
        $v = array_change_key_case($v);
        if (empty($option)) {
            $columns[] = $v["field"];
        } else {
            $vv = array();
            foreach ($option as $op) {
                $op = strtolower($op);
                if (array_key_exists($op, $v)) {
                    $vv[$op] = $v[$op];
                }
            }
            $columns[$v["field"]] = $vv;
        }
    }
    return $columns;
}
//用法
//dump(getcolumns("ggw",array("key","type")));
