<?php

namespace app\index\controller;

use app\common\model\dictionary;
use app\common\model\dimension;
use think\facade\Request;
use app\common\model\config;


class Index
{

    protected $dimension = null;

    function __construct()
    {
        $this->dimension = dimension::getDimension();
    }



    public function home()
    {
        $keys["key"] = "key";
        $keys = array_merge($keys, $this->dimension);
        $distinck = config::distinctConfig(array_keys($keys));

        $_ret = [];
        $c = 4;
        $i = 0;
        $_ks = [];
        foreach ($keys as $k => $v) {
            $i++;
            if ($i <= $c) {
                $_ks[$k] = $v;
            } else {
                $_ret[] = $_ks;
                $i = 0;
                $_ks = [];
                $_ks[$k] = $v;
            }

        }
        if ($_ks) $_ret[] = $_ks;

        return view("", ["menu_item_index" => "1", "distinctConfig" => $distinck, "kss" => $_ret]);

    }

    public function configmanage()
    {
        $dimension = $this->dimension;
        $selectdata = Request::param();
        $columnNames = array_keys($dimension);
        $columnNames[] = "key";
        $selectdata = array_only_key($selectdata, $columnNames);
        $selectdata["page"] = 1;

        return view("", ["menu_item_index" => "2", "dimension" => $dimension, "selectdata" => $selectdata]);
    }

    public function updateconfig()
    {

        $dimension = $this->dimension;
        $keys["key"] = "key";
        $keys = array_keys( array_merge($keys, $dimension));

        $id = Request::param("id");
        $dimension = $this->dimension;
        $list = config::selectConfig(["id" => $id], $dimension);
        $config = array_get($list, 0, []);
        $value_type = dictionary::get_("value_type");

        return view("", ["menu_item_index" => "2", "dimensionList" => dimension::getDimensionList() , "config" => $config, "vts" => $value_type, "keys"=>$keys]);
    }

    public function createconfig()
    {
        $dimension = $this->dimension;
        $keys["key"] = "key";
        $keys = array_keys( array_merge($keys, $dimension));

        $value_type = dictionary::get_("value_type");
        return view("", ["menu_item_index" => "2", "dimensionList" => dimension::getDimensionList(), "vts" => $value_type, "keys"=>$keys]);
    }

    public function valuetest()
    {
        $dimension = $this->dimension;
        $keys["key"] = "key";
        $keys = array_merge($keys, $dimension);
        $distinck = config::distinctConfig(array_keys($keys));

        foreach ($distinck as  $k=>&$item1){
            $distinck[$k]= array_column($item1,$k);
        }

        $dimensionList= dimension::getDimensionList();

        foreach ($dimensionList as &$item){
            $key=$item["key"];
            $values= array_get($item,"values_array",[]) ;
            $_vs=array_get($distinck,$key,[]) ;

            $distinck[$key]=  array_unique( array_merge([""],$values, $_vs));

        }

        $selectData=Request::param();
        $selectData=array_only_key($selectData,array_keys($keys));
        $selectData["_"]="_";
        return view("", ["menu_item_index" => "3", "distinctConfig" => $distinck, "ks" => $keys, "dimension" => $dimension,"select_Data"=>$selectData,"keys"=>array_keys( $keys)]);

    }

    public function set()
    {

        return view("", ["menu_item_index" => "4"]);
    }
}
