<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2015 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------
namespace tests;

use app\common\model\config;
use app\common\model\dictionary;
use app\common\model\dimension;

class dbDictionaryTest extends TestCase
{

    public function test_dictionary()
    {
        //var_dump(dictionary::all()->toArray());

        $b = dictionary::all()->count() > 0;
        $this->assertTrue($b);
    }

    public function test_getDimension()
    {
        $v = dimension::getDimension();
        //var_dump(count($v));
        //var_dump(json_encode($v, JSON_UNESCAPED_UNICODE));
        //$b=count($v)>0;
        $this->assertTrue(count($v) > 0);
    }



    public function test_checkDimension(){

        $ar=config::checkDimension(["dfsdfsdf","app"]);
        //var_dump($ar);
        $this->assertEquals(["dfsdfsdf"], $ar);
    }

    /*public function test_set()
    {
        $str = '{"app":"应用","runmodel":"运行方式"}';
        dictionary::setDimension(json_decode($str, true));
        $this->assertTrue(true);

    }*/

}