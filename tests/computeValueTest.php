<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2015 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------
namespace tests;

use app\common\model\config;
use app\common\model\dictionary;
use app\common\model\dimension;

class computeValueTest extends TestCase
{

    private $conf = [
        ["id" => 1, "key" => "tkey", "value" => 111, "value_type" => "number", "app" => "", "runmodel" => ""],
        ["id" => 2, "key" => "tkey", "value" => 222, "value_type" => "number", "app" => "ap", "runmodel" => ""],
        ["id" => 3, "key" => "tkey", "value" => 333, "value_type" => "number", "app" => "ap", "runmodel" => "de"],
        ["id" => 4, "key" => "tkey1", "value" => '444${tkey}', "value_type" => "string", "app" => "", "runmodel" => ""],
        ["id" => 5, "key" => "tkey2", "value" => '555${tkey12}', "value_type" => "string", "app" => "", "runmodel" => ""],

    ];
    private $dimension = ["app", "runmodel"];

    public function test_config_err()
    {
        //$this->expectExceptionMessage('config is null');
        $obj = new \computeValue($this->dimension);


        $this->assertEquals(null,$obj->compute("tkey", []));

    }

    public function test_key_err()
    {
        $this->expectExceptionMessage('key error');
        $obj = new \computeValue($this->dimension);
        $obj->compute("", []);

    }

    public function test_dimension_err()
    {
        $this->expectExceptionMessage('dimension error');

        $dimension = $this->dimension;
        $dimension[] = "addadd";

        $obj = new \computeValue($dimension);
        $obj->compute("tkey", $this->conf, ["app" => "ap", "runmodel" => "re"]);

    }

    /*public function test_dimensionValues_err()
    {
        $this->expectExceptionMessage('dimensionValues error');

        $dimension = $this->dimension;

        $obj = new \computeValue($dimension);
        $obj->compute("tkey", $this->conf);

    }*/

    public function test_value()
    {

        $dimension = $this->dimension;
        $obj = new \computeValue($dimension);
        $this->assertEquals(111, $obj->compute("tkey", $this->conf, ["app" => "app", "runmodel" => "re"])["value"]);
        $this->assertEquals(222, $obj->compute("tkey", $this->conf, ["app" => "ap", "runmodel" => "re"])["value"]);
        $this->assertEquals(333, $obj->compute("tkey", $this->conf, ["app" => "ap", "runmodel" => "de"])["value"]);

        $this->assertEquals(333, $obj->compute("tkey", $this->conf, ["app" => "ap", "runmodel" => "de","sdfsdf"=>"sdfsd" ])["value"]);
        $this->assertEquals(111, $obj->compute("tkey", $this->conf, [])["value"]);

        $this->assertEquals(222, $obj->compute("tkey", $this->conf, ["app" => "ap", ])["value"]);

        $_conf = [
            ["id" => 2, "key" => "tkey", "value" => 222, "value_type" => "number", "app" => "ap", "runmodel" => ""],
            ["id" => 3, "key" => "tkey", "value" => 333, "value_type" => "number", "app" => "ap", "runmodel" => "de"],
        ];
        $this->assertEquals(null, $obj->compute("tkey", $_conf, ["app" => "app", ]));
        ///var_dump( $obj->compute("tkey",$this->conf ));
        ///$this->assertTrue(true);
    }

    public function test_getVarFromContent()
    {

        $str = 'sdf${dsds}sdfsds${111}';
        $arr = getVarFromContent($str);

        //var_dump(json_encode($arr));

        $ret = contentFormat($str, ["dsds" => 3232323, "111" => "kkkkkkkkkkkkkkkkkk"]);
        $this->assertEquals($ret, "sdf3232323sdfsdskkkkkkkkkkkkkkkkkk");
        //var_dump( $ret );

        // $this->assertTrue(true);
    }

    public function test_get(){

        $dimension = $this->dimension;
        $obj = new \computeValue($dimension);
        $this->assertEquals(null, $obj->get("tkey12",[], function(){ return  $this->conf;} ));

        $this->assertEquals("444111", $obj->get("tkey1",[], function(){ return  $this->conf;} )["value"]);
        $this->assertEquals('555${tkey12}', $obj->get("tkey2",[], function(){ return  $this->conf;} )["value"]);


    }

    public function test_get1(){

        $tempkey = "temp" . (string)rand(1000000, 9999999);
        $v= "v" . (string)rand(1000000, 9999999);
        config::insertConfig(["key"=>$tempkey,"value"=>$v], dimension::getDimension(), dimension::getDimensionList());
        $_conf= config::selectConfig(["key"=>$tempkey,"value"=>$v ], dimension::getDimension());
        $this->assertTrue(count($_conf) > 0);

        $id=$_conf[0]["id"];
        config::updateConfig(["id"=>$id,"key"=>$tempkey,"value"=>$v."update"], dimension::getDimension());


        $obj = new \computeValue(array_keys(dimension::getDimension()),function($item){config::setLook($item);}  );
        $ret = $obj->get($tempkey, [], function ($key) {
            return config::getConfigByKey($key);
        });
        $ret = $obj->get($tempkey, [], function ($key) {
            return config::getConfigByKey($key);
        });
        $this->assertEquals($ret["value"], $v."update" );

        config::delConfig($id);


    }


}

